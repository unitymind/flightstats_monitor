/*
 * Serve content over a socket
 */

var _ = require('underscore')._;
var async = require('async');
var Shred = require("shred");
var shred = new Shred();
var moment = require('moment');
var config = require('../libs/config');
var apiHeaders = config.get('flightStats');
var baseUrl = 'https://api.flightstats.com/flex/flightstatus/rest/v2/json/airport/status/';
var dateTimeFormat =  'DD.MM.YYYY HH:mm';
var dateFormat = 'DD.MM.YYYY';

_.templateSettings = {
  interpolate : /\{\{(.+?)\}\}/g
};

var buildQuery = function(tParams) {
  var uri = _.template('{{airport}}/{{direction}}/{{year}}/{{month}}/{{day}}/{{hourOfDay}}');
  return baseUrl + uri(tParams);

};
var statusExplained =  {
  'A': 'Active',
  'C':	'Canceled',
  'D':	'Diverted',
  'DN':	'Data source needed',
  'L':	'Landed',
  'NO':	'Not Operational',
  'R':	'Redirected',
  'S':	'Scheduled',
  'U':	'Unknown'
};

var filterResults = function(results) {
  var filtered = [];
  _.each(results.flightStatuses, function(status) {
    var combined = {
      flightNumber: status.carrierFsCode + ' ' + status.flightNumber,
      departureCode:  status.departureAirportFsCode,
      arrivalCode: status.arrivalAirportFsCode,
      status: statusExplained[status.status]
    }
    combined.airlineName = _.findWhere(results.appendix.airlines, {fs: status.carrierFsCode}).name;
    combined.departureName = _.findWhere(results.appendix.airports, {fs: status.departureAirportFsCode}).name;
    combined.arrivalName = _.findWhere(results.appendix.airports, {fs: status.arrivalAirportFsCode}).name;
    if (!!status.codeshares) {
      combined.codeshares = _.map(status.codeshares, function(info) { return info.fsCode + ' ' + info.flightNumber}).join(', ');
    }
    if (!!status.airportResources && !!status.airportResources.departureGate) {
      combined.gate = status.airportResources.departureGate;
    }

    if (!status.operationalTimes.publishedDeparture) {
      if(!!status.operationalTimes.scheduledGateDeparture) {
        combined.scheduledDeparture = moment(status.operationalTimes.scheduledGateDeparture.dateLocal).format(dateTimeFormat);
        combined.departureDate = moment(status.operationalTimes.scheduledGateDeparture.dateLocal).format(dateFormat);
      } else if (!!status.operationalTimes.flightPlanPlannedDeparture) {
        combined.scheduledDeparture = moment(status.operationalTimes.flightPlanPlannedDeparture.dateLocal).format(dateTimeFormat);
        combined.departureDate = moment(status.operationalTimes.flightPlanPlannedDeparture.dateLocal).format(dateFormat);
      }
    } else {
      combined.scheduledDeparture = moment(status.operationalTimes.publishedDeparture.dateLocal).format(dateTimeFormat);
      combined.departureDate = moment(status.operationalTimes.publishedDeparture.dateLocal).format(dateFormat);
    }

    if (!status.operationalTimes.publishedArrival) {
      if(!!status.operationalTimes.scheduledGateArrival) {
        combined.scheduledArrival = moment(status.operationalTimes.scheduledGateArrival.dateLocal).format(dateTimeFormat);
      } else if (!!status.operationalTimes.flightPlanPlannedArrival) {
        combined.scheduledArrival = moment(status.operationalTimes.flightPlanPlannedArrival.dateLocal).format(dateTimeFormat);
      }
    } else {
      combined.scheduledArrival = moment(status.operationalTimes.publishedArrival.dateLocal).format(dateTimeFormat);
    }

    if (!!status.operationalTimes.actualGateDeparture) {
      combined.actualDeparture = moment(status.operationalTimes.actualGateDeparture.dateLocal).format(dateTimeFormat);
    }

    if (!!status.operationalTimes.actualRunwayArrival) {
      combined.actualArrival = moment(status.operationalTimes.actualRunwayArrival.dateLocal).format(dateTimeFormat);
    } else if (!!status.operationalTimes.actualGateArrival) {
      combined.actualArrival = moment(status.operationalTimes.actualGateArrival.dateLocal).format(dateTimeFormat);
    }

    filtered.push(combined);
  });
  return filtered;
}

var sortByDate = function(array, field) {
  return array.sort(function(a, b){
    var dateA = new Date(a[field]).getTime();
    var dateB = new Date(b[field]).getTime();
    if (dateA == dateB) {
      return 0;
    }
    return dateA > dateB ? 1 : -1;
  });
}

var stopTimeout = function(noMsg, socket) {
  if (!!socket.store.data['requestTimeout']) {
    _.forEach(socket.store.data['requestTimeout'], clearTimeout);
    socket.set('requestTimeout', [], function(){
      if (!noMsg) {
        socket.emit('send:message', {msg: 'Live update is stopped on: ' + moment().format('MMMM Do YYYY, HH:mm:ss'), type: 'warning'});
      }
    });
  }
}


exports.stop = function(req) {
    stopTimeout(req.data.noMsg, req.socket);
}

exports.query = function (req) {
  req.socket.set('requestTimeout', []);
  req.socket.set('query', req.data.params);
  req.socket.set('updateInterval', parseInt(req.data.updateInterval));

  var startDate;
  var qParams = { utc: 'true', codeType: 'IATA' };
  var tParams = _.clone(req.data.params);

  var executeRequests = function() {
    var asyncRequests = {};
    var iterWhile = 0;
    if (!!req.data.params.carrier) {
      startDate = moment.utc().subtract('hours', 12);
      qParams.carrier = req.data.params.carrier;
      qParams.numHours = 2;
      iterWhile = 11;
    } else {
      startDate = moment.utc().subtract('hours', 4);
      qParams.numHours = 2;
      iterWhile = 3;
    }

    for (var i = 0; i <= iterWhile; i++) {
      var currentDate = startDate.clone().add('hours', qParams.numHours * i);
      tParams.year = currentDate.year();
      tParams.month = currentDate.month() + 1;
      tParams.day = currentDate.day() + 1;
      tParams.hourOfDay = currentDate.hour();
      asyncRequests[i] =
        (function() {
          var requestUri = buildQuery(tParams);
          return function(callback){
            shred.get({
              url: requestUri,
              headers: apiHeaders,
              query: qParams,
              on: {
                success: function(response) {
                  if (!!response.content.data.error) {
                    callback(response.content.data.error);
                  } else {
                    callback(null, response.content.data);
                  }
                },
                error: function(response) {
                  callback({errorMessage: "Connection problem!"});
                }
              }
            });
          }
        })();
    }

    async.parallel(asyncRequests,
        function(err, results){
          if (!!err) {
            if (err instanceof Error) {
              req.socket.emit('send:message', {msg: 'Server side problem. Try later!', type: 'error'});
            } else {
              req.socket.emit('send:message', {msg: err.errorMessage, type: 'error'});
            }
            console.log(err);
          }
          else {
            if (!req.socket.disconnected) {
              stopTimeout(true, req.socket);

              var statuses = _.flatten(_.map(results, filterResults));
              var sorted = tParams.direction == 'arr' ? sortByDate(statuses, 'scheduledArrival') : sortByDate(statuses, 'scheduledDeparture');

              req.socket.emit('send:message', {msg: 'Last updated on: ' + moment().format('MMMM Do YYYY, HH:mm:ss'), type: 'notice'});
              req.socket.emit('send:statuses', {data: sorted});

              var timeouts = req.socket.store.data['requestTimeout'];
              timeouts.push(setTimeout(executeRequests, req.socket.store.data['updateInterval'] * 1000))
              req.socket.set('requestTimeout', timeouts);
            } else {
              console.log("DEAD SOCKET!!!!!!");
            }
          }

        }
    );
  };

  executeRequests();

};

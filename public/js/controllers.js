'use strict';

/* Controllers */

angular.module('flightStats.controllers', []).
  controller('AppCtrl', function ($scope, socket) {
    socket.on('send:handshake', function (data) {
      console.log("Websocket connection: " + data.msg);
    });
  }).
  controller('SearchFormCtrl', function ($scope, socket) {
    $scope.isBusy = false;
    $scope.updateInterval = '2';
    $scope.currentQuery = {};
    $scope.statuses = [];
    $scope.searchFormData = { direction: 'arr'};

    $scope.submit = function() {
      $scope.statuses = [];
      $scope.currentQuery = $scope.searchFormData;
      socket.emit('submit:query', { params: $scope.searchFormData, updateInterval: $scope.updateInterval});
      $scope.isBusy = true;
      angular.element('#messageBox').removeClass().addClass('notice').text('Request in progress...');
    }

    $scope.stop = function() {
      console.log("Stop called!");
      socket.emit('submit:stop', {noMsg: false});
      $scope.statuses = [];
      $scope.isBusy = false;
    }

    socket.on('send:message', function (data) {
      angular.element('#messageBox').removeClass().addClass(data.type).text(data.msg);
      console.log(data.msg);
    });

    socket.on('send:statuses', function (statuses) {
      if (!$scope.isBusy) {
        socket.emit('submit:stop', {noMsg: true});
      } else {
        $scope.statuses = statuses.data;
      }
    });
  });

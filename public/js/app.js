'use strict';

// Declare app level module which depends on filters, and services

angular.module('flightStats', [
  'flightStats.controllers',
  // 3rd party dependencies
  'btford.socket-io'
]);
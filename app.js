
/**
 * Module dependencies
 */

var express = require('express.io'),
  routes = require('./routes'),
  path = require('path');

var app = module.exports = express();
app.http().io();

/**
 * Configuration
 */

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

// development only
if (app.get('env') === 'development') {
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
}

// production only
if (app.get('env') === 'production') {
    app.use(express.errorHandler());
};


/**
 * Routes
 */

var handlers = require('./routes/handlers');

// serve index and view partials
app.get('/', routes.index);
app.io.route('submit:query', handlers.query);
app.io.route('submit:stop', handlers.stop);

/**
 * Start Server
 */


app.listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
